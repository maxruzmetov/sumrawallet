//Core
import React, { Component } from 'react';
import { loginActions } from '../actions';
import { connect } from 'react-redux';

//Api
import { api } from '../../../api';

/**
 * type State
 */
type Props = {
  /**
   * Function change step authentification
   */
  onStep: Function;
  className?: string;
  save: (token: string) => any;
};

/**
 * type State
 */
type State = {
  /**
   * User name
   */
  username: string;

  /**
   * Password
   */
  password: string;
};

/**
 * The Login form  rendering the login page.
 *
 * @extends Component
 */
class LoginForm extends Component<Props, State> {
  /**
   * constructor
   */
  constructor(props: Props) {
    super(props);

    //Rewrite to empty before build
    this.state = {
      username: 'SumraEduard',
      password: 'VvN1zRx16H',
    };
  }

  /**
   * render
   */
  render() {
    let { className } = this.props;
    className += ' login-form';

    return (
      <div className={className}>
        <h1 className="h1-title">Login with Sumra ID</h1>

        <form onSubmit={this._onFormSubmit}>
          <fieldset className="sumra-input-fieldset">
            <legend>User name</legend>

            <img
              className="sumra-input-fieldset-icon"
              src="images/sumra/icon-person.svg"
              alt="img"
            />

            <input
              type="text"
              placeholder="Enter username"
              value={this.state.username}
              onChange={this._changeUserName}
            />
          </fieldset>

          <fieldset className="sumra-input-fieldset">
            <legend>Password</legend>

            <img
              className="sumra-input-fieldset-icon"
              src="images/sumra/icon-lock.svg"
              alt="img"
            />

            <input
              type="text"
              placeholder="Enter password"
              value={this.state.password}
              onChange={this._changePassword}
            />
          </fieldset>

          <button className="sumra-Button" type="submit">
            <img
              className="sumra-Button-icon-left"
              src="images/sumra/icon-logout.svg"
              alt="img"
              width="18"
            />
            <span>Sign up</span>
          </button>

          <div className="sumra-link-forgotPassword">Forgot password?</div>
          <div className="sumra-link-createUser">
            New user?
            <span onClick={this._goToRegistration}>Create a Sumra ID</span>
          </div>
        </form>
      </div>
    );
  }

  /**
   * Handler input user name
   *
   * @param {Event} event - The HTML Event which details the form submission.
   * @private
   * @returns {void}
   */
  _changeUserName = (event) => {
    this.setState({ username: event.target.value });
  };

  /**
   * Handler input password
   *
   * @param {Event} event - The HTML Event which details the form submission.
   * @private
   * @returns {void}
   */
  _changePassword = (event) => {
    this.setState({ password: event.target.value });
  };

  /**
   * Prevents submission of the form and sign up user.
   *
   * @param {Event} event - The HTML Event which details the form submission.
   * @private
   * @returns {void}
   */
  _onFormSubmit = (event) => {
    event.preventDefault();

    if (this.state.username && this.state.password) {
      this._signIn();
    }
  };

  /**
   * Create user
   * @private
   * @returns {void}
   */
  _signIn = async () => {
    const { username, password } = this.state;
    const { save } = this.props;

    const { access_token } = await api.auth.login({ username, password });

    const { localStorage } = window;

    localStorage.setItem('access_token', access_token);

    save(access_token);

    // this.props.loadByDate(access_token)
    // dispatch(loginActions.setUserToken());

    // location.href = 'https://meet.sumra.net/defaultRoom?jwt=' + meet_token;
  };

  /**
   * Back to the first step, registration
   * @private
   * @returns {void}
   */
  _goToRegistration = () => {
    this.props.onStep(1);
  };
}

const mapStateToProps = (state) => ({ ...state.login });

const mapDispatchToProps = (dispatch) => ({
  save: (token: string) => {
    dispatch(loginActions.setUserToken(token));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
