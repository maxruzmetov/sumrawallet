// Core
import { loginActions } from '../../components/login/actions';
import { useSelector, useDispatch } from 'react-redux';

export const useNavigation = () => {
  const dispatch = useDispatch();

  const { userToken } = useSelector((state) => state.auth);

  const logoutHandler = () => {
    dispatch(loginActions.setUserToken(null));
    localStorage.removeItem('access_token');
  };

  return { isAuthenticated: !!userToken, logoutHandler };
};
