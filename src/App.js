import React from 'react';
import { Router } from 'react-router-dom';

import { Provider } from 'react-redux';
import { store } from './init/store';

import { Routes } from './navigation';
import { history } from './navigation/history';

export const App = () => {
  return (
    <>
      <Provider store={store}>
        <Router history={history}>
          <Routes />
        </Router>
      </Provider>
    </>
  );
};
