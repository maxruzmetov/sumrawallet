// Core
import { combineReducers } from 'redux';

// Reducers
import { authReducer as auth } from '../components/login/reducer';

export const rootReducer = combineReducers({
  auth,
});
