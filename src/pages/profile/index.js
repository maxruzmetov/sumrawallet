import React from 'react';

import { Dashboard as DashboardComponent } from '../../components/dashboard';

export const Dashboard = () => {
  return <DashboardComponent />;
};
