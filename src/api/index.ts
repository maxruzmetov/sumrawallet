export const END_POINTS = {
  SERVER: 'https://api.sumra.net/',
  SEND_CODE: 'auth/v1/send-code',
  VALIDATE: 'auth/v1/validate',
  REGISTRATION: 'auth/v1/registration',
  AUTHENTIFICATION: 'auth/v1/meet/authenticate',
};

// const apiToken = process.env.API_CONSUMER_KEY;
const baseUrl = 'https://api.sumra.net';

const apiToken = 'TGUzX0JpbWJCaWZlWWExOFpZNlVTejFSOWZzYTpfOE5MbmNqMjBxQjRDekVmUjlpRElOQ3dmejhh';

export const api = Object.freeze({
  auth: {
    async login({
      username,
      password,
    }: {
      username: string;
      password: string;
    }): Promise<{
      access_token: string;
      refresh_token: string;
      token_type: 'Bearer';
      expires_in: number;
    }> {
      const resp = await fetch(`${baseUrl}/token`, {
        method: 'POST',
        body: String(
          new URLSearchParams({
            username,
            password,
            grant_type: 'password',
          })
        ),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Basic ${apiToken}`
        },
      });

      const data = await resp.json();

      return data;
    },

    async ge({
      username,
      password,
    }: {
      username: string;
      password: string;
    }): Promise<{
      access_token: string;
      refresh_token: string;
      token_type: 'Bearer';
      expires_in: number;
    }> {
      const resp = await fetch(`${baseUrl}/token`, {
        method: 'POST',
        body: String(
          new URLSearchParams({
            username,
            password,
            grant_type: 'password',
          })
        ),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Basic ${apiToken}`
        },
      });

      const data = await resp.json();

      return data;
    },

    makeFetch: (action, data) => {
      return fetch(END_POINTS.SERVER + action, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
    },
    fetchValidateName: (name) => {
      return fetch(
        END_POINTS.SERVER + END_POINTS.VALIDATE + '?username=' + name
      );
    },
    fetchAuth: (data) => {
      return fetch(END_POINTS.SERVER + END_POINTS.AUTHENTIFICATION, {
        method: 'POST',
        // mode: 'no-cors',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
    },
    generetaId(length: number): string {
      var result = '';
      var characters =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        );
      }
      return result;
    },
  },
});

// export const api = Object.freeze({
//     users: {
//         getMe: () => {
//             return fetch(`${root}/profile`, {
//                 method: 'GET',
//                 credentials:'include'
//             });
//         },
//         create: (payload) => {
//             return fetch(`${root}/users`, {
//                 method: 'POST',
//                 headers: {
//                     'Content-Type': 'application/json'
//                 },
//                 body: JSON.stringify(payload),
//             });
//         },
//         updateMe: (payload) => {
//             return fetch(`${root}/users`, {
//                 method: 'PUT',
//                 headers: {
//                     'Content-Type': 'application/json'
//                 },
//                 credentials:'include',
//                 body: JSON.stringify(payload),
//             });
//         },
//         login: (credentials) => {
//             return fetch(`${root}/login`, {
//                 method: 'POST',
//                 headers: {
//                     'Content-Type': 'application/json',
//                     'authorization': `Base ${credentials}`
//                 },
//                 credentials:'include'
//             });
//         },
//         logout: () => {
//             return fetch(`${root}/logout`, {
//                 method: 'POST',
//                 credentials:'include'
//             });
//         },
//     },
//     tracker: {
//         getScore: () => {
//             return fetch(`${root}/reports`, {
//                 method: 'GET',
//                 credentials:'include'
//             });
//         },
//         getRecord: ({type}) => {
//             const params = new URLSearchParams({
//                 kind: type,
//             });

//             return fetch(`${root}/records?${params}`, {
//                 method: 'GET',
//                 credentials:'include'
//             });
//         },
//         createRecord: ({type, record}) => {
//             const params = new URLSearchParams({
//                 kind: type,
//             });

//             return fetch(`${root}/records?${params}`, {
//                 method: 'POST',
//                 headers: {
//                     'Content-Type': 'application/json'
//                 },
//                 credentials:'include',
//                 body: JSON.stringify({
//                     value: record
//                 }),
//             });
//         },
//         updateRecord: ({type, hash, record}) => {
//             const params = new URLSearchParams({
//                 kind: type,
//             });

//             return fetch(`${root}/records/${hash}/?${params}`, {
//                 method: 'PUT',
//                 headers: {
//                     'Content-Type': 'application/json'
//                 },
//                 credentials:'include',
//                 body: JSON.stringify({
//                     value: record
//                 }),
//             });
//         },
//         removeAllRecords: () => {
//             return fetch(`${root}/reports/reset`, {
//                 method: 'POST',
//                 credentials:'include'
//             });
//         },
//     }
// });
