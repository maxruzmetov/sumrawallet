//Core
import React from 'react';
import AuthPage from './components/AuthPage';

//Styles
import './styles.scss';

export const Login = () => {

  return (
    <section>
      <AuthPage />
    </section>
  );
};
