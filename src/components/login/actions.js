import { types } from './types';

export const loginActions = {
  setUserToken: (token) => {
    return {
      type: types.SET_USER_TOKEN,
      payload: token
    };
  },
};
