//Core
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { book } from './book';

//Components
// import { Header } from '../components/header';
import { Login } from '../pages/login';
import { Dashboard } from '../pages/dashboard';
import { Header } from '../components/header';
// import { Profile } from '../pages/profile';

//Hooks
import { useNavigation } from './hooks/useNavigation';

export const Routes = () => {
  const { isAuthenticated } = useNavigation();

  return (
    <>
      <Header />
      <Switch>
        <Route exact path={book.root}>
          {isAuthenticated ?  <Dashboard /> : <Login />}
        </Route>
        <Route exact path={book.dashboard}>
          {isAuthenticated ?  <Dashboard /> : <Redirect to={book.root} />}
        </Route>
      </Switch>
    </>
  );
};



// <Switch>
//   <Route exact path={book.root}>
//     {loggedIn ? <Redirect to={book.profile} /> : <Registration />}
//   </Route>
//   <Route exact path={book.login}>
//     {loggedIn ? <Redirect to={book.profile} /> : <Login />}
//   </Route>
//   <Route exact path={book.profile}>
//     {loggedIn ? <Profile /> : <Redirect to={book.login} />}
//   </Route>
// </Switch>
