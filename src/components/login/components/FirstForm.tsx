//Core
import React, { Component } from 'react';

//Api
import { END_POINTS, api } from '../../../api';

//Components
import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input';
import flags from 'react-phone-number-input/flags';

type Props = {
  /**
   * Function to step on click.
   */
  onStep: Function;

  /**
   * className
   */
  className?: string;
  targetBlank?: string;
  socialLinkWidth?: number;
  socialLinks?: Array<{ image: string; href: string }>;
};

type State = {
  /**
   * Phone number
   *
   */
  phone: string;
};

export class FirstForm extends Component<Props, State> {
  static defaultProps = {
    targetBlank: '_blank',
    socialLinkWidth: 46,
    socialLinks: [
      {
        image: 'Telegram',
        href: 'https://t.me/sumrabot',
      },
      {
        image: 'Viber',
        href: '#',
      },
      {
        image: 'Messanger',
        href: '#',
      },
      {
        image: 'WhatsApp',
        href: '#',
      },
      {
        image: 'Signal',
        href: '#',
      },
    ],
  };

  /**
   * Initializes a new {@code FirstForm} instance.
   *
   * @param {Props} props - The React {@code Component} props to initialize
   * the new {@code FirstForm} instance with.
   */
  constructor(props) {
    super(props);

    this.state = {
      phone: '',
    };
  }

  /**
   * Render
   *
   * @private
   * @returns {void}
   */
  render() {
    const { className, socialLinks, socialLinkWidth, targetBlank } = this.props;
    const links = socialLinks.map((v, index) => {
      const src = 'images/sumra/' + v.image + '.svg';

      return (
        <li key={index} onClick={this._goToVeryfycationCodePage}>
          <a href={v.href} target={targetBlank}>
            <img src={src} width={socialLinkWidth} alt="img" />
          </a>
        </li>
      );
    });

    return (
      <div className={className}>
        <h1 className="h1-title">Wellcome to Sumra Chat</h1>
        <h2 className="h2-subtitle">Please Login or Sign Up</h2>
        <section>
          <h3 className="h3-label">Sign up with:</h3>

          <ul className="sumra-social-links">{links}</ul>
        </section>
        <div className="sumra-line"></div>
        <section>
          <h3 className="h3-label">Sign up with:</h3>
          <form>
            <fieldset className="sumra-phone-fieldset">
              <legend>Your mobile phone number</legend>

              <PhoneInput
                flags={flags}
                placeholder="Enter phone number"
                value={this.state.phone}
                onChange={this._changePhoneNumber}
              />

              <div
                className="sumra-phone-send"
                onClick={this._submitPhoneNumber}
              >
                <img src="images/sumra/send.svg" alt="img" />
              </div>
            </fieldset>
          </form>
        </section>
        <div className="sumra-line"></div>
        <div className="sumra-Button" onClick={this._goToLoginPage}>
          <img src="images/sumra/user.svg" alt="img" width="14" height="17" />
          <span>Login with Sumra ID</span>
        </div>
        <img
          className="sumra-Benefits-draft"
          src="images/sumra/Benefits_draft.svg"
          alt="img"
        />
      </div>
    );
  }

  /**
   * Set state after changed phone number.
   *
   * @param {String} phone - The HTML Event which details the form submission.
   * @private
   * @returns {void}
   */
  _changePhoneNumber = (phone) => {
    this.setState({ phone });
  };

  /**
   * _goToLoginPage
   */
  _goToLoginPage = () => {
    this.props.onStep(4);
  };

  /**
   * _goToVeryfycationCodePage
   */
  _goToVeryfycationCodePage = () => {
    this.props.onStep(2);
  };

  /**
   * Submit form value (phone number).
   *
   * @param {Event} event - The HTML Event which details the form submission.
   * @private
   * @returns {void}
   */
  _submitPhoneNumber = (event) => {
    event.preventDefault();

    let { phone } = this.state;

    if (!phone) {
      return;
    }

    phone = phone.replace('+', '');

    api.auth
      .makeFetch(END_POINTS.SEND_CODE, {
        phone_number: phone,
        device_id: api.auth.generetaId(20),
      })
      .then(
        (response) => console.log,
        (error) => console.error
      );
    this.props.onStep(2);
  };
}
