//Core
import React from 'react';
import Styles from './style.module.scss';
import { Link } from 'react-router-dom';
import { book } from '../../navigation/book';

//Hooks
import { useNavigation } from '../../navigation/hooks/useNavigation';

export const Header = () => {
  const { isAuthenticated, logoutHandler } = useNavigation();
  
  // const loggedIn = true;

  const logoutLinkJSX = isAuthenticated && (
    <Link to={book.root} onClick={logoutHandler}>
      Logout
    </Link>
  );

  return (
    <section className={Styles.header}>
      <nav>
        {logoutLinkJSX}
      </nav>
    </section>
  );
};
