import { types } from './types';

const initialState = {
  userToken: localStorage.getItem('access_token') || null,
};

export const authReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SET_USER_TOKEN:
      return {
        ...state,
        userToken: payload,
      };
    default:
      return state;
  }
};
