//Core
import React from 'react';


export const Sidebar: React.FC = () => {

  return (
    <section>
      <h2>Sidebar</h2>
      <ul>
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 3</li>
        <li>Item 4</li>
      </ul>
    </section>
  );
};
