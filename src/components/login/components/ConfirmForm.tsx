import React, { Component, createRef } from 'react';
import ReactCodeInput from 'react-verification-code-input';

type Props = {
  /**
   * Function to step on click.
   */
  onStep: Function;

  /**
   * Function set Verification code to upper state
   */
  onSetCode: Function;

  className?: string;

  type?: any;
  fields?: number;
  fieldWidth?: number;
  fieldHeight?: number;
};

type State = {
  verificationCode: string;
};

export class ConfirmForm extends Component<Props, State> {
  input = createRef<any>();

  state = {
    verificationCode: '',
  };

  static defaultProps = {
    autoFocus: true,
    fieldWidth: 38,
    fieldHeight: 44,
    type: 'text',
    fields: 6,
  };

  /**
   * Render
   *
   * @private
   * @returns {void}
   */
  render() {
    const {
      type,
      fieldWidth,
      fieldHeight,
      // fields
    } = this.props;

    let { className } = this.props;

    className += ' verification-code-form';

    return (
      <div className={className}>
        <h1 className="h1-title">Confirmation Access</h1>

        <form>
          <h2 className="h2-label">Enter the six-digit verification code.</h2>

          <ReactCodeInput
            className="sumra-react-code-input"
            ref={this.input}
            type={type}
            fieldWidth={fieldWidth}
            fieldHeight={fieldHeight}
            onChange={this._handleChange}
            onComplete={this._handleComplete}
          />

          <button
            className="sumra-Button"
            onClick={this._submitVerificationCode}
          >
            <img
              className="sumra-Button-icon-left"
              src="images/sumra/icon-logout.svg"
              alt="img"
              width="18"
            />

            <span>Continue</span>
          </button>
        </form>
      </div>
    );
  }

  /**
   * Submit verification code.
   *
   * @param {Event} event - The HTML Event which details the form submission.
   * @protected
   * @returns {void}
   */
  _submitVerificationCode = (event) => {
    event.preventDefault();

    const { verificationCode } = this.state;
    const { fields } = this.props;
    const isComplete = verificationCode.length === fields;

    if (isComplete) {
      this.props.onSetCode(verificationCode);
      this.props.onStep(3);
    }

    localStorage.setItem('verification_code', JSON.stringify(verificationCode));
  };

  /**
   * _handleChange
   */
  _handleChange = (vals) => {
    console.log('handleChange: ' + vals);
  };

  /**
   * _handleComplete
   */
  _handleComplete = (verificationCode) => {
    this.setState({ verificationCode });
  };
}
