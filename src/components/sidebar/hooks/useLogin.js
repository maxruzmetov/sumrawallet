// Core
import { loginActions } from '../actions';
import { useDispatch } from 'react-redux';

//Hooks
import { useTime } from './useTime';

//Nav
import { useHistory } from 'react-router-dom';
import { book } from '../../../navigation/book';

export const useLogin = () => {
  const dispatch = useDispatch();

  const blockWaitTime = 5;

  const { onIntervalEvent, timeLeft, setTimeLeft } = useTime();

  const user = JSON.parse(localStorage.getItem('loginData'));

  let loginRedirect = useHistory();

  const loginEqual = (loginForm, user) => {
    return (
      user.login === loginForm.login && user.password === loginForm.password
    );
  };

  const failureHandler = () => {
    const totalFails = Number(localStorage.getItem('failsCounter'));

    if (totalFails != null) {
      if (totalFails === 2) {
        const blockerTimer = Math.round(
          new Date().getTime() / 1000 + blockWaitTime
        );

        localStorage.setItem('blockerTimer', blockerTimer);
        localStorage.removeItem('failsCounter');

        setTimeLeft(blockWaitTime);

        const timer = setInterval(() => {
          onIntervalEvent(timer);
        }, 1000);
      } else {
        localStorage.setItem('failsCounter', totalFails + 1);
      }
    } else {
      localStorage.setItem('failsCounter', 1);
    }
  };

  const successHandler = () => {
    dispatch(loginActions.setLoginStatus(true));
    loginRedirect.push(book.profile);
  };

  const sendForm = (loginForm) => {
    if (loginEqual(loginForm, user)) {
      successHandler();
    } else {
      failureHandler();
    }
  };

  return {
    sendForm,
    isFormBlocked: timeLeft > 0,
    timeLeft,
  };
};
